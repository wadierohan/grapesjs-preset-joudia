export default (editor, config) => {
    const sm = editor.StyleManager;
    const csm = config.customStyleManager;

    editor.on('load', function () {
        sm.getSectors().reset(csm && csm.length ? csm : [
            {
                name: config.textGeneral,
                open: false,
                buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom'],
                properties: [
                    {
                        property: 'display',
                        list: [
                            { value: 'block', name: 'block' },
                            { value: 'inline', name: 'inline' },
                            { value: 'inline-block', name: 'inline-block'  },
                            { value: 'flex', name: 'flex' },
                            { value: 'inline-flex', name: 'inline-flex' },
                            { value: 'table', name: 'table' },
                            { value: 'table-cell', name: 'table-cell' },
                            { value: 'table-column', name: 'table-column' },
                            { value: 'table-row', name: 'table-row' },
                            { value: 'none', name: 'none' },
                        ],
                    }
                ]
            },{
                name: config.textLayout,
                open: false,
                buildProps: ['min-width', 'min-height', 'width', 'height', 'max-width', 'max-height', 'margin', 'padding'],
            },{
                name: config.textTypography,
                open: false,
                buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-shadow'],
                properties: [
                    {
                        property: 'text-align',
                        list: [
                            { value: 'left', className: 'fa fa-align-left' },
                            { value: 'center', className: 'fa fa-align-center'  },
                            { value: 'right', className: 'fa fa-align-right' },
                            { value: 'justify', className: 'fa fa-align-justify' },
                        ],
                    },
                ]
            },{
                name: config.textDecorations,
                open: false,
                buildProps: ['background-color', 'background', 'border-radius-c', 'border-radius', 'border', 'box-shadow'],
            },{
                name: config.textExtra,
                open: false,
                buildProps: ['cursor', 'transition', 'perspective', 'transform'],
            }
        ]);
    });
}
