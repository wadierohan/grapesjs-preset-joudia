export default (editor, config) => {
    return {
        run(editor) {
            let params = {'_method': 'PATCH', '_token': config.csrf, 'html': editor.getHtml(), 'css': editor.getCss()}
            let form = document.createElement("form");
            form.setAttribute("method", "POST");
            form.setAttribute("action", config.submitUrl);

            for(let key in params) {
                if(params.hasOwnProperty(key)) {
                    let hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                }
            }
            window.onbeforeunload = null;
            document.body.appendChild(form);
            form.submit();
        },

        stop() {

        }
    }
}
