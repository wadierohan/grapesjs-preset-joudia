import openImport from './openImport';
import save from './save';
import {
  cmdImport,
  cmdDeviceDesktop,
  cmdDeviceTablet,
  cmdDeviceMobile,
  cmdDeviceLandscape,
  cmdClear,
  cmdSave,
  cmdExit
} from './../consts';

export default (editor, config) => {
  const cm = editor.Commands;
  const txtConfirm = config.textCleanCanvas;
  const textExit = config.textExit;

  cm.add(cmdImport, openImport(editor, config));
  cm.add(cmdDeviceDesktop, e => e.setDevice('Desktop'));
  //cm.add(cmdDeviceTablet, e => e.setDevice('Real Tablet'));
  cm.add(cmdDeviceMobile, e => e.setDevice('Real Mobile portrait'));
  cm.add(cmdDeviceLandscape, e => e.setDevice('Real Mobile landscape'));
  cm.add(cmdClear, e => confirm(txtConfirm) && e.runCommand('core:canvas-clear'));
  cm.add(cmdSave, save(editor, config));
  cm.add(cmdExit, e => confirm(textExit) ? window.location.href = config.exitUrl : '');
}
