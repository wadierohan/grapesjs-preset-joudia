import grapesjs from 'grapesjs';
import pluginBlocks from 'grapesjs-blocks-basic-joudia';
import pluginNavbar from 'grapesjs-navbar';
import pluginCountdown from 'grapesjs-component-countdown';
import pluginForms from 'grapesjs-plugin-forms-joudia';
import pluginExport from 'grapesjs-plugin-export';

import pluginTabs from 'grapesjs-tabs';
import pluginSlider from 'grapesjs-lory-slider';
import pluginTooltip from 'grapesjs-tooltip';
import pluginParser from 'grapesjs-parser-postcss';
import pluginCustomcode from 'grapesjs-custom-code';
import pluginTouch from 'grapesjs-touch';

import commands from './commands';
import blocks from './blocks';
import components from './components';
import panels from './panels';
import styles from './styles';

export default grapesjs.plugins.add('gjs-preset-joudia', (editor, opts = {}) => {
  let config = opts;

  let defaults = {
    // Which blocks to add
    blocks: ['link-block', 'quote', 'text-basic', 'divider', 'grid-items', 'list-items'],

    // Modal import title
    modalImportTitle: 'Import',

    // Modal import button text
    modalImportButton: 'Import',

    // Import description inside import modal
    modalImportLabel: '',

    // Default content to setup on import model open.
    // Could also be a function with a dynamic content return (must be a string)
    // eg. modalImportContent: editor => editor.getHtml(),
    modalImportContent: '',

    // Code viewer (eg. CodeMirror) options
    importViewerOptions: {},

    // Confirm text before cleaning the canvas
    textCleanCanvas: 'Êtes vous sûr de vouloir réinitialiser le canvas?',

    // Confirm text before Exiting the canvas
    textExit: 'Êtes vous sûr de vouloir quitter sans sauvegarder?',

    // Title of icons
    titleDesktop: 'Grand écran',
    titleMobile: 'Phone portrait',
    titleLandscape: 'Phone paysage',
    titleSwv: 'Contour',
    titlePreview: 'Prévisualiser',
    titleFullscreen: 'Plein écran',
    titleCode: 'Code',
    titleUndo: 'Annuler',
    titleRedo: 'Rétablir',
    titleImport: 'Importer',
    titleClear: 'Réinitialiser',
    titleSave: 'Sauvegarder',
    titleExit: 'Quitter',
    titleStyle: 'Style',
    titleConfig: 'Configuration',
    titleComposition: 'Composition',
    titleElements: 'Éléments',

    // Show the Style Manager on component change
    showStylesOnChange: 1,

    // Text for General sector in Style Manager
    textGeneral: 'General',

    // Text for Layout sector in Style Manager
    textLayout: 'Dimension',

    // Text for Typography sector in Style Manager
    textTypography: 'Typography',

    // Text for Decorations sector in Style Manager
    textDecorations: 'Decorations',

    // Text for Extra sector in Style Manager
    textExtra: 'Extra',

    // Use custom set of sectors for the Style Manager
    customStyleManager: [],

    // `grapesjs-blocks-basic` plugin options
    // By setting this option to `false` will avoid loading the plugin
    blocksBasicOpts: {},

    // `grapesjs-navbar` plugin options
    // By setting this option to `false` will avoid loading the plugin
    navbarOpts: {},

    // `grapesjs-component-countdown` plugin options
    // By setting this option to `false` will avoid loading the plugin
    countdownOpts: {},

    // `grapesjs-plugin-forms` plugin options
    // By setting this option to `false` will avoid loading the plugin
    formsOpts: {
        'joudiaTags': [],
        'joudiaFields': [{value:'', name:'Choisir un champs'},{value:'email', name:'Email'}],
        'joudiaFormSubmitUrl': '',
    },

    // `grapesjs-plugin-export` plugin options
    // By setting this option to `false` will avoid loading the plugin
    exportOpts: {},

    // `grapesjs-tabs` plugin options
    // By setting this option to `false` will avoid loading the plugin
    tabsOpts: {
        tabsBlock: {
            category: 'Extra'
        }
    },

    // `grapesjs-lory-slider` plugin options
    // By setting this option to `false` will avoid loading the plugin
    sliderOpts: {
        sliderBlock: {
            category: 'Extra'
        }
    },

    // `grapesjs-tooltip` plugin options
    // By setting this option to `false` will avoid loading the plugin
    tooltipOpts: {},

    // `grapesjs-parser-postcss` plugin options
    // By setting this option to `false` will avoid loading the plugin
    parserOpts: {},

    // `grapesjs-custom-code` plugin options
    // By setting this option to `false` will avoid loading the plugin
    customcodeOpts: {},

    // `grapesjs-touch` plugin options
    // By setting this option to `false` will avoid loading the plugin
    touchOpts: {},

    // Url of submit
    submitUrl: '',

    // Exit URL
    exitUrl: '/'
  };

  // Load defaults
  for (let name in defaults) {
    if (!(name in config))
      config[name] = defaults[name];
  }

  const {
    blocksBasicOpts,
    navbarOpts,
    countdownOpts,
    formsOpts,
    exportOpts,
    tabsOpts,
    sliderOpts,
    tooltipOpts,
    parserOpts,
    customcodeOpts,
    touchOpts
  } = config;

  // Load plugins
  blocksBasicOpts && pluginBlocks(editor, blocksBasicOpts);
  navbarOpts && pluginNavbar(editor, navbarOpts);
  countdownOpts && pluginCountdown(editor, countdownOpts);
  formsOpts && pluginForms(editor, formsOpts);
  exportOpts && pluginExport(editor, exportOpts);
  tabsOpts && pluginTabs(editor, tabsOpts);
  sliderOpts && pluginSlider(editor, sliderOpts);
  tooltipOpts && pluginTooltip(editor, tooltipOpts);
  parserOpts && pluginParser(editor, parserOpts);
  customcodeOpts && pluginCustomcode(editor, customcodeOpts);
  touchOpts && pluginTouch(editor, touchOpts);

  // Load components
  components(editor, config);

  // Load blocks
  blocks(editor, config);

  // Load commands
  commands(editor, config);

  // Load panels
  panels(editor, config);

  // Load styles
  styles(editor, config);

});
