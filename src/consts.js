export const
  cmdImport = 'gjs-open-import-webpage',
  cmdDeviceDesktop = 'set-device-desktop',
  cmdDeviceTablet = 'set-device-tablet',
  cmdDeviceMobile = 'set-device-mobile',
  cmdDeviceLandscape = 'set-device-landscape',
  cmdClear = 'canvas-clear',
  cmdSave = 'save',
  cmdExit = 'exit'
