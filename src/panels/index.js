import {
  cmdImport,
  cmdDeviceDesktop,
  cmdDeviceTablet,
  cmdDeviceMobile,
  cmdDeviceLandscape,
  cmdClear,
  cmdSave,
  cmdExit
} from './../consts';

export default (editor, config) => {
  const pn = editor.Panels;
  const eConfig = editor.getConfig();
  const crc = 'create-comp';
  const mvc = 'move-comp';
  const swv = 'sw-visibility';
  const expt = 'export-template';
  const osm = 'open-sm';
  const otm = 'open-tm';
  const ola = 'open-layers';
  const obl = 'open-blocks';
  const ful = 'fullscreen';
  const prv = 'preview';

  eConfig.showDevices = 0;

  pn.getPanels().reset([{
    id: 'commands',
    buttons: [{}],
  },{
    id: 'options',
    buttons: [{
        active: true,
        id: swv,
        command: swv,
        context: swv,
        className: 'fa fa-square-o',
        attributes: {
            title: config.titleSwv,
        }
    },{
      id: prv,
      context: prv,
      command: e => e.runCommand(prv),
      className: 'fa fa-eye',
      attributes: {
          title: config.titlePreview,
      }
    },{
      id: ful,
      command: ful,
      context: ful,
      className: 'fa fa-arrows-alt',
      attributes: {
          title: config.titleFullscreen,
      }
    },{
      id: expt,
      className: 'fa fa-code',
      command: e => e.runCommand(expt),
      attributes: {
          title: config.titleCode,
      }
    },{
      id: 'undo',
      className: 'fa fa-undo',
      command: e => e.runCommand('core:undo'),
      attributes: {
          title: config.titleUndo,
      }
    },{
      id: 'redo',
      className: 'fa fa-repeat',
      command: e => e.runCommand('core:redo'),
      attributes: {
          title: config.titleRedo,
      }
    },{
      id: cmdImport,
      className: 'fa fa-download',
      command: e => e.runCommand(cmdImport),
      attributes: {
          title: config.titleImport,
      }
    },{
      id: cmdClear,
      className: 'fa fa-trash',
      command: e => e.runCommand(cmdClear),
      attributes: {
          title: config.titleClear,
      }
    },{
      id: cmdSave,
      className: 'fa fa-floppy-o',
      command: e => e.runCommand(cmdSave),
      attributes: {
          title: config.titleSave,
      }
    },{
      id: cmdExit,
      className: 'fa fa-sign-out',
      command: e => e.runCommand(cmdExit),
      attributes: {
          title: config.titleExit,
      }
    }],
  },{
    id: 'views',
    buttons  : [{
      id: osm,
      command: osm,
      active: true,
      className: 'fa fa-paint-brush',
      attributes: {
          title: config.titleStyle,
      }
    },{
      id: otm,
      command: otm,
      className: 'fa fa-cog',
      attributes: {
          title: config.titleConfig,
      }
    },{
      id: ola,
      command: ola,
      className: 'fa fa-bars',
      attributes: {
          title: config.titleComposition,
      }
    },{
      id: obl,
      command: obl,
      className: 'fa fa-th-large',
      attributes: {
          title: config.titleElements,
      }
    }],
  }]);

    // Add devices buttons
    const panelDevices = pn.addPanel({id: 'devices-c'});
    panelDevices.get('buttons').add([
        {
            id: cmdDeviceDesktop,
            command: cmdDeviceDesktop,
            className: 'fa fa-desktop',
            active: 1,
            attributes: {
                title: config.titleDesktop,
            }
        },/*{
            id: cmdDeviceTablet,
            command: cmdDeviceTablet,
            className: 'fa fa-tablet',
        },*/{
            id: cmdDeviceMobile,
            command: cmdDeviceMobile,
            className: 'fa fa-mobile',
            attributes: {
                title: config.titleMobile,
            }
        },{
            id: cmdDeviceLandscape,
            command: cmdDeviceLandscape,
            className: 'fa fa-mobile fa-rotate-90',
            attributes: {
                title: config.titleLandscape,
            }
        }
    ]);

  const openBl = pn.getButton('views', obl);
  editor.on('load', () => openBl && openBl.set('active', 1));

  // On component change show the Style Manager
  config.showStylesOnChange && editor.on('component:selected', () => {
    const openTmBtn = pn.getButton('views', otm);
    const openSmBtn = pn.getButton('views', osm);
    const openLayersBtn = pn.getButton('views', ola);

    // Don't switch when the Layer Manager is on or
    // there is no selected component
    if ((!openLayersBtn || !openLayersBtn.get('active')) && (!openSmBtn || !openSmBtn.get('active')) && editor.getSelected()) {
      openTmBtn && openTmBtn.set('active', 1);
    }
  });
}
