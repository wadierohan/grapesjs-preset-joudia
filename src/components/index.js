export default (editor, config = {}) => {
  const domc = editor.DomComponents;
  const defaultType = domc.getType('default');
  const defaultModel = defaultType.model;
  const defaultView = defaultType.view;
  const deviceManager = editor.DeviceManager;
  // ...
  deviceManager.add('Real Tablet', '768px', {
       height: '1024px',
       widthMedia: '768px', // the width that will be used for the CSS media
   });
   deviceManager.add('Real Mobile portrait', '414px', {
       height: '736px',
       widthMedia: '414px', // the width that will be used for the CSS media
   });
   deviceManager.add('Real Mobile landscape', '736px', {
       height: '414px',
       widthMedia: '736px', // the width that will be used for the CSS media
   });
}
