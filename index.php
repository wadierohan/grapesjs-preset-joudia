<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>GrapesJS Preset Joudia</title>
    <link href="https://unpkg.com/grapesjs/dist/css/grapes.min.css" rel="stylesheet">
    <link href="dist/grapesjs-preset-joudia.min.css" rel="stylesheet">
    <script src="https://unpkg.com/grapesjs"></script>
    <script src="dist/grapesjs-preset-joudia.min.js"></script>
    <style>
      body,
      html {
        height: 100%;
        margin: 0;
      }
    </style>
  </head>
  <body>

    <div id="gjs">
        <style>
            .gjs-row{
                display:table;
                padding:10px;
                width:100%;
            }
            .gjs-cell{
                width:8%;
                display:table-cell;
                height:75px;
            }
            @media (max-width: 768px){
                .gjs-cell{
                    width:100%;
                    display:block;
                }
            }
        </style>
        <div class="gjs-row"><div class="gjs-cell"></div></div>
    </div>

    <script type="text/javascript">
      var editor = grapesjs.init({
            storageManager: {
                id: 'gjs-',             // Prefix identifier that will be used inside storing and loading
                type: 'local',          // Type of the storage
                autosave: true,         // Store data automatically
                autoload: true,         // Autoload stored data on init
                stepsBeforeSave: 1,     // If autosave enabled, indicates how many changes are necessary before store method is triggered
                storeComponents: true, // Enable/Disable storing of components in JSON format
                storeStyles: true,     // Enable/Disable storing of rules/style in JSON format
                storeHtml: true,        // Enable/Disable storing of components as HTML string
                storeCss: true,         // Enable/Disable storing of rules/style as CSS string
            },
            protectedCss: '',
            height: '100%',
            showOffsets: 1,
            noticeOnUnload: 0,
            //storageManager: { autoload: 0 },
            container: '#gjs',
            fromElement: true,

            plugins: ['gjs-preset-joudia'],
            pluginsOpts: {
              'gjs-preset-joudia': {
                  'blocksBasicOpts':{
                      flexGrid: true,
                      stylePrefix: 'gjs-',
                      addBasicStyle: true,
                  },
                  'submitUrl': 'test',
                  'exitUrl': 'https://google.com',
                  'formsOpts': {
                      'joudiaTags': [{value:'1', name:'Tag 1'},{value:'2', name:'Tag 2'},{value:'3', name:'Tag 3'}],
                      'joudiaFields': [{value:'', name:'Choisir un champs'},{value:'email', name:'Email'},{value:'field-1', name:'Field 1'},{value:'field-2', name:'Field 2'},{value:'field-3', name:'Field 3'}],
                      'joudiaFormSubmitUrl': '',
                  }
              },
            }
      });
      console.log(editor.StorageManager.getStorages())
    </script>
  </body>
</html>
